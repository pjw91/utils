__version__ = '0.0.1'
__all__ = ['atomic_write']

from .wrapper_atomicwrites import atomic_write
