import contextlib
import os

import atomicwrites
atomicwrites._proper_fsync = lambda fd: None


@contextlib.contextmanager
def atomic_write(path, **kwargs):
    """Same as atomicwrites.atomic_write(), but restores st_mtime if content is unchanged."""
    try:
        with open(path, 'rb') as f:
            content = f.read()
        stat = os.stat(path)
    except FileNotFoundError:
        content = None

    with atomicwrites.atomic_write(path, **kwargs) as f:
        yield f
    with open(path, 'rb') as f:
        if f.read() == content:
            os.utime(path, ns=(stat.st_atime_ns, stat.st_mtime_ns))
