import os
import unittest
import time

from atomicwrites import atomic_write as aw

from pjw91 import atomic_write as my_aw


class Test(unittest.TestCase):
    def setUp(self):
        with open('test_aaa', 'xb') as f:
            f.write(b'aaa')
        with open('test_bbb', 'xb') as f:
            f.write(b'bbb')
        self.mt_a = os.stat('test_aaa').st_mtime_ns
        self.mt_b = os.stat('test_bbb').st_mtime_ns
        time.sleep(1)

    def tearDown(self):
        os.unlink('test_aaa')
        os.unlink('test_bbb')

    def test_changed(self):
        with aw('test_aaa', mode='wb', overwrite=True) as f:
            f.write(b'bbb')
        with my_aw('test_bbb', mode='wb', overwrite=True) as f:
            f.write(b'aaa')

        self.assertNotEqual(self.mt_a, os.stat('test_aaa').st_mtime_ns)
        self.assertNotEqual(self.mt_b, os.stat('test_bbb').st_mtime_ns)

    def test_unchanged(self):
        with aw('test_aaa', mode='wb', overwrite=True) as f:
            f.write(b'aaa')
        with my_aw('test_bbb', mode='wb', overwrite=True) as f:
            f.write(b'bbb')

        self.assertNotEqual(self.mt_a, os.stat('test_aaa').st_mtime_ns)
        self.assertEqual(self.mt_b, os.stat('test_bbb').st_mtime_ns)

    def test_newfile(self):
        with aw('new_aaa', mode='wb') as f:
            f.write(b'aaa')
        with my_aw('new_bbb', mode='wb') as f:
            f.write(b'bbb')
        os.unlink('new_aaa')
        os.unlink('new_bbb')
